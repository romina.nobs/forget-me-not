import express from 'express'
import dotenv from 'dotenv'
import connectDb from './utils/db.mjs'
import { Server } from 'socket.io'
import cors from 'cors'
import bodyParser from 'body-parser'
import http from 'http'
import { getReminders } from './services/reminder.service.mjs'
import { getPictures } from './services/picture.service.mjs'
import { updateReminder } from './services/reminder.service.mjs'

dotenv.config()
connectDb()

const app = express()
const port = process.env.port
const express_port = process.env.port_express

app.use(express.json())
app.use(cors())
//app.use(bodyParser)

//Update Reminder
app.put('/reminders/:id', async function (req, res) {
  console.log(req.body)

  const result = await updateReminder(req.body._id, req.body)
  res.send(result)
})

app.listen(express_port, () => {
  console.log(`Example app listening at http://localhost:${express_port}`)
})

const server = http.createServer(app)
const io = new Server(server, { cors: { orign: '*' } })

server.listen(8000, () => {
  console.log('Socket.io läuft auf 8000')
})

const listReminder = async () => {
  try {
    const reminders = await getReminders()
    io.emit('get_reminders', reminders)
  } catch (err) {
    console.error(err)
  }
}

const listPicture = async () => {
  try {
    const pictures = await getPictures()
    io.emit('get_pictures', pictures)
  } catch (err) {
    console.error(err)
  }
}

const updatePicture = () => {
  // TODO: update Reminder return updated one
  // try {
  //   const reminders = await updatePicture()
  //   io.emit('update_picture', reminders)
  // } catch (err) {
  //   console.error(err)
  // }
}

io.on('connection', socket => {
  console.log('A user connected')

  socket.on('reminder:update', updateReminder)
  socket.on('reminder:list', listReminder)

  socket.on('picture:update', updatePicture)
  socket.on('picture:list', listPicture)
})

app.use(express.static('public'))
