import mongoose from 'mongoose'
const { Schema } = mongoose

export const ReminderSchema = new Schema(
  {
    title: {
      type: String,
      required: true
    },
    active: {
      type: Boolean,
      required: true
    },
    notification: {
      // time: {
      //   type: Date
      // },
      // tone: {
      //   type: String
      // },
      // vibration: {
      //   type: Boolean
      // },
      allowNotificationFrom: {
        type: String
      },
      allowNotificationUntil: {
        type: String
      },
      interval: {
        type: Number
      },
      days: {
        monday: {
          type: Boolean
        },
        tuesday: {
          type: Boolean
        },
        wednesday: {
          type: Boolean
        },
        thursday: {
          type: Boolean
        },
        friday: {
          type: Boolean
        },
        saturday: {
          type: Boolean
        },
        sunday: {
          type: Boolean
        }
      }
    }
  },
  { timestamps: true }
)

export default mongoose.model('reminder', ReminderSchema, 'reminder')
